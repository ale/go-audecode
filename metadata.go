package audecode

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/unicode/norm"
)

var (
	knownTagsMap map[string]struct{}
	knownTags    = []string{
		"album",
		"album_artist",
		"artist",
		"comment",
		"composer",
		"copyright",
		"creation_time",
		"date",
		"disc",
		"disctotal",
		"encoder",
		"encoded_by",
		"filename",
		"genre",
		"language",
		"performer",
		"publisher",
		"service_name",
		"service_provider",
		"title",
		"track",
		"tracktotal",
		"variant_bitrate",
	}

	dateFormats = []string{
		time.RFC3339,
		time.RFC822,
		"2006/01/02",
		"2006-01-02",
		"02/01/2006",
		"02-01-2006",
		"2006",
	}
)

func normalizeDate(value string) (string, error) {
	for _, fmt := range dateFormats {
		if t, err := time.Parse(fmt, value); err == nil {
			return t.Format(time.RFC3339), nil
		}
	}
	return "", errors.New("could not parse date")
}

// NormalizeMetadata will sanitize metadata tags from a song and
// return a new map with the result.
func NormalizeMetadata(meta map[string]string) map[string]string {
	out := make(map[string]string)
	for tag, value := range meta {
		if !isKnownTag(tag) {
			continue
		}

		// Attempt Unicode normalization.
		value = norm.NFC.String(value)

		value = strings.TrimSpace(value)

		// Split N/N track tag into track and tracktotal.
		if tag == "track" && strings.Contains(value, "/") {
			parts := strings.Split(value, "/")
			value = parts[0]
			out["tracktotal"] = parts[1]
		}

		// Normalize values for some well-known tags.
		switch tag {
		case "track", "disc", "tracktotal", "disctotal":
			value = strings.TrimLeft(value, "0")

		case "genre":
			value = strings.ToLower(value)

		case "date", "creation_time":
			var err error
			value, err = normalizeDate(value)
			if err != nil {
				continue
			}
		}

		out[tag] = value
	}
	return out
}

func isKnownTag(t string) bool {
	_, ok := knownTagsMap[t]
	return ok
}

func toMap(l []string) map[string]struct{} {
	out := make(map[string]struct{})
	for _, s := range l {
		out[s] = struct{}{}
	}
	return out
}

func init() {
	knownTagsMap = toMap(knownTags)
}

// MetadataString formats song metadata into a nice descriptive string.
func MetadataString(meta map[string]string) string {
	var parts []string
	addWithSep := func(sep, s string) {
		if len(parts) > 0 {
			parts = append(parts, sep)
		}
		parts = append(parts, s)
	}

	title, ok := meta["title"]
	if !ok {
		title = "Unknown Song"
	}
	parts = append(parts, title)

	artist, ok := meta["artist"]
	if ok {
		addWithSep(", ", artist)
	}

	album, ok := meta["album"]
	if ok {
		if artistAlbum, ok := meta["artist_album"]; ok && artist != artistAlbum {
			album = fmt.Sprintf("%s - %s", album, artistAlbum)
		}
		if disc, ok := meta["disc"]; ok {
			discN, _ := strconv.Atoi(disc)
			var totalN int
			if total, ok := meta["disctotal"]; ok {
				totalN, _ = strconv.Atoi(total)
				disc = fmt.Sprintf("%s/%s", disc, total)
			}
			if totalN > 1 || discN > 1 {
				album = fmt.Sprintf("%s %s", album, disc)
			}
		}
		addWithSep(" - ", album)
	}

	if track, ok := meta["track"]; ok {
		if total, ok := meta["tracktotal"]; ok {
			track = fmt.Sprintf("%s/%s", track, total)
		}
		addWithSep(", ", fmt.Sprintf("track %s", track))
	}

	return strings.Join(parts, "")
}

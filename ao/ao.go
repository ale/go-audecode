package ao

/*
#cgo LDFLAGS: -lao
#include <ao/ao.h>
*/
import "C"
import (
	"errors"
	"syscall"
	"unsafe"
)

func init() {
	C.ao_initialize()
}

type SampleFormat struct {
	SampleRate     int
	Channels       int
	BytesPerSample int
}

type Device struct {
	inner *C.ao_device
}

func aoError(err error) error {
	if errno, ok := err.(syscall.Errno); ok {
		switch int(errno) {
		case C.AO_ENODRIVER:
			return errors.New("no such audio driver")
		case C.AO_ENOTLIVE:
			return errors.New("not a valid live output device")
		case C.AO_EBADOPTION:
			return errors.New("bad option in audio device config")
		case C.AO_EOPENDEVICE:
			return errors.New("error opening audio device")
		}
	}
	return err
}

func New(driver string, format SampleFormat, options map[string]string) (*Device, error) {
	var aoDriver C.int
	if driver == "" {
		aoDriver = C.ao_default_driver_id()
	} else {
		aoDriver = C.ao_driver_id(C.CString(driver))
		if aoDriver < 0 {
			return nil, errors.New("unknown audio device driver")
		}
	}

	var aoFormat C.ao_sample_format
	aoFormat.rate = C.int(format.SampleRate)
	aoFormat.channels = C.int(format.Channels)
	aoFormat.bits = C.int(format.BytesPerSample * 8)
	aoFormat.byte_format = C.AO_FMT_LITTLE

	var aoOpts *C.ao_option
	C.ao_append_option(&aoOpts, C.CString("debug"), C.CString("true"))
	for key, value := range options {
		C.ao_append_option(&aoOpts, C.CString(key), C.CString(value))
	}
	defer C.ao_free_options(aoOpts)

	dev, err := C.ao_open_live(C.int(aoDriver), &aoFormat, aoOpts)
	if dev == nil {
		return nil, aoError(err)
	}
	return &Device{inner: dev}, nil
}

func (d *Device) Close() error {
	if 0 == C.ao_close(d.inner) {
		return errors.New("error closing audio device")
	}
	return nil
}

func (d *Device) Write(p []byte) (int, error) {
	if 0 == C.ao_play(d.inner, (*C.char)(unsafe.Pointer(&p[0])), C.uint_32(len(p))) {
		return 0, errors.New("play error")
	}
	return len(p), nil
}

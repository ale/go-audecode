
go-audecode
===========

Go bindings for [audecode](https://git.autistici.org/ale/audecode), a
simple interface to libavcodec/libavformat.

package audecode

/*
#cgo pkg-config: audecode
#include <audecode.h>
extern audecode_read_callback_t readCallbackPtr;
*/
import "C"

import (
	"fmt"
	"io"
	"strings"
	"unsafe"
)

func init() {
	C.audecode_init()
}

type Audecode struct {
	reader io.Reader
	inner  C.audecode_t
}

type Params struct {
	SampleRate     int
	Channels       int
	BytesPerSample int
}

//export readCallback
func readCallback(opaque unsafe.Pointer, buf *C.uchar, sz C.int) C.int {
	au := (*Audecode)(opaque)
	gobuf := (*[1 << 30]byte)(unsafe.Pointer(buf))[:sz:sz]
	n, err := au.reader.Read(gobuf)
	if err != nil || n == 0 {
		return C.int(-1)
	}
	return C.int(n)
}

func newError(msg string, err C.int) error {
	var buf [256]byte
	C.audecode_strerror(err, (*C.char)(unsafe.Pointer(&buf[0])), C.int(256))
	return fmt.Errorf("%s: %s", msg, string(buf[:]))
}

// New returns a new Audecode object wrapping the given io.Reader. The
// returned object can't be used for reading until SetOutputParams has
// been called successfully.
func New(r io.Reader) (*Audecode, error) {
	au := Audecode{reader: r}
	if err := C.audecode_new(&au.inner, 4096, C.readCallbackPtr, unsafe.Pointer(&au)); err != 0 {
		return nil, newError("audecode_new", err)
	}
	return &au, nil
}

// Close releases all resources associated with the Audecode object.
func (au *Audecode) Close() error {
	C.audecode_close(au.inner)
	return nil
}

// Read matches the io.Reader interface.
func (au *Audecode) Read(out []byte) (int, error) {
	n := C.audecode_read(au.inner, (*C.uint8_t)(unsafe.Pointer(&out[0])), C.int(len(out)))
	if n < 0 {
		return 0, newError("audecode_read", n)
	}
	return int(n), nil
}

// GetInputParams returns the audio parameters of the input stream.
func (au *Audecode) GetInputParams() Params {
	var sampleRate, channels, bps C.int
	C.audecode_get_input_params(au.inner, &sampleRate, &channels, &bps)
	return Params{
		SampleRate:     int(sampleRate),
		Channels:       int(channels),
		BytesPerSample: int(bps),
	}
}

// GetInputCodec returns the name of the input audio codec.
func (au *Audecode) GetInputCodec() string {
	var codec *C.char
	C.audecode_get_input_codec_params(au.inner, &codec, nil)
	return C.GoString(codec)
}

// GetInputBitRate returns the bit rate of the input audio codec (if
// applicable).
func (au *Audecode) GetInputBitRate() int {
	var bitrate C.int
	C.audecode_get_input_codec_params(au.inner, nil, &bitrate)
	return int(bitrate)
}

// SetOutputParams set the desired output parameters.
func (au *Audecode) SetOutputParams(p Params) error {
	r := C.audecode_set_output_params(au.inner, C.int(p.SampleRate), C.int(p.Channels), C.int(p.BytesPerSample))
	if r < 0 {
		return newError("audecode_set_output_params", r)
	}
	return nil
}

// Metadata returns the song metadata as a key/value map.
func (au *Audecode) Metadata() map[string]string {
	md := make(map[string]string)
	var ckey, cvalue *C.char
	iter := C.audecode_get_metadata(au.inner)
	for {
		if r := C.audecode_metadata_next(iter, &ckey, &cvalue); r != 0 {
			break
		}
		key := strings.ToLower(C.GoString(ckey))
		md[key] = C.GoString(cvalue)
	}
	return md
}

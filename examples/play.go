package main

import (
	"flag"
	"io"
	"log"
	"os"

	"git.autistici.org/ale/go-audecode"
	"git.autistici.org/ale/go-audecode/ao"
)

var inputFile = flag.String("input", "", "input file")

func aoSampleFormat(p audecode.Params) ao.SampleFormat {
	return ao.SampleFormat{
		SampleRate:     p.SampleRate,
		Channels:       p.Channels,
		BytesPerSample: p.BytesPerSample,
	}
}

func main() {
	flag.Parse()

	r, err := os.Open(*inputFile)
	if err != nil {
		log.Fatal(err)
	}

	au, err := audecode.New(r)
	if err != nil {
		log.Fatal(err)
	}
	defer au.Close()

	params := au.GetInputParams()
	log.Printf("input params: %+v", params)
	if err := au.SetOutputParams(params); err != nil {
		log.Fatal(err)
	}

	meta := audecode.NormalizeMetadata(au.Metadata())
	for key, value := range meta {
		log.Printf("metadata %s = %s", key, value)
	}
	log.Printf("song: %s", audecode.MetadataString(meta))
	log.Printf("input codec: %s", au.GetInputCodec())
	log.Printf("input bitrate: %d", au.GetInputBitRate())

	audiodev, err := ao.New("", aoSampleFormat(params), nil)
	if err != nil {
		log.Fatal(err)
	}
	defer audiodev.Close()

	io.Copy(audiodev, au)
}
